<define-tag pagetitle>Debian 6.0 <q>Squeeze</q> 正式發行</define-tag>
<define-tag release_date>2011-02-05</define-tag>
#use wml::debian::news

<p>經過 24 個月不停的開發，Debian 計畫很榮幸在此宣佈推出新的穩定版：
Debian 6.0 (代號 <q>Squeeze</q>)。Debian 6.0 作為一個自由的作業係統，這是第
一次推第二種不同的風味；除了常見的 Debian GNU/Linux，這次還引入了 Debian
GNU/kFreeBSD 作<q>技術預覽</q>。</p>

<p>Debian 6.0 包含了 KDE Plasma 桌面及應用程式、GNOME、Xfce 和 LXDE 桌
面環境，還有各種不同的伺服器程式。此外，它兼容 FHS v2.3，可以適用相容
LSB 3.2 的各式軟體程式。</p>

<p>Debian 可以執行於各種電腦上，從掌上型電腦與手持係統到超級電腦，幾乎
所有包括所有的類型。Debian GNU/Linux 目前總共支援九種硬體架構：32-bit
PC / Intel IA-32 (<code>i386</code>), 64-bit PC / Intel EM64T / x86-64
(<code>amd64</code>), Motorola/IBM PowerPC (<code>powerpc</code>),
Sun/Oracle SPARC (<code>sparc</code>), MIPS (<code>mips</code>
(big-endian) and <code>mipsel</code> (little-endian)), Intel Itanium
(<code>ia64</code>), IBM S/390 (<code>s390</code>), ARM EABI
(<code>armel</code>).</p>

<p>Debian 6.0 <q>Squeeze</q> 引入兩個來自 FreeBSD 計畫的核心作為技術預
覽，使用知名的 Debian/GNU userland：Debian GNU/kFreeBSD，分別是 32-bit
PC 的 <code>kfreebsd-i386</code> 和 64-bit PC 的
<code>kfreebsd-amd64</code>。這些是 Debian 有史以來各版本中第一個不是基
於 Linux 核心的版本。對於一般伺服器軟體的支援已經很完整，結合原本
Linux 版本的特性以及來自 BSD 世界獨有的特色。但這個新版本仍有所限制；例
如一些進階的桌面支援就沒有包含進去。</p>

<p>這次也是第一次附帶完全自由的 Linux 核心，完全沒有包含問題多多的韌體
檔案。這些檔案已經分到獨自的套件並移出 Debian 主檔案庫，改放到預設不使
用的 non-free 檔案庫。這樣一來 Debian 使用者就有機會選擇使用完全自由的
作業係統，必要的時候也可以選擇使用 non-free 的韌體檔。安裝過程中需要的
韌體檔可以從安裝媒體中讀取；也可以從特製的 CD 映像檔以及 USB 碟使用的檔
案取得。更多的資訊可以參考 Debian <a
href="http://wiki.debian.org/Firmware">韌體維基網頁</a>。</p>

<p>還有更多，Debian 6.0 使用新的使用相依性計算啟動順序的開機係統，讓開機速
度更快，更穩固，全因為用了平行執行的開機腳本以及正確的相依性追蹤。其他
改變如 KDE Plasma Netbook shell 則讓 Debian 更適合安裝在小型的
notebook 上。</p>

<p>這一版本包含了數量巨大的軟件包更新，例如：
<ul>
<li>KDE Plasma Workspaces 和 KDE Applications 4.4.5</li>
<li>GNOME 桌面環境 2.30 的維護更新</li>
<li>Xfce 4.6 桌面環境</li>
<li>LXDE 0.5.0</li>
<li>X.Org 7.5</li>
<li>OpenOffice.org 3.2.1</li>
<li>GIMP 2.6.11</li>
<li>Iceweasel 3.5.16 (無商標版本的 Mozilla Firefox)</li>
<li>Icedove 3.0.11 (無商標版本的 Mozilla Thunderbird)</li>
<li>PostgreSQL 8.4.6</li>
<li>MySQL 5.1.49</li>
<li>GNU Compiler Collection (GCC) 4.4.5</li>
<li>Linux 2.6.32</li>
<li>Apache 2.2.16</li>
<li>Samba 3.5.6</li>
<li>Python 2.6.6, 2.5.5 和 3.1.3</li>
<li>Perl 5.10.1</li>
<li>PHP 5.3.3</li>
<li>Asterisk 1.6.2.9</li>
<li>Nagios 3.2.3</li>
<li>Xen Hypervisor 4.0.1 (支持 dom0 和 domU)</li>
<li>OpenJDK 6b18</li>
<li>Tomcat 6.0.18</li>
<li>以及其他構建自約 15,000 個源代碼包的超過 29,000 個開箱即用的軟件包。</li>
</ul>
Debian 6.0 包含了包括 Chromium 瀏覽器、Icinga 監控解決方案、軟件中心、Wicd 網絡管理器、Linux 
容器工具 lxc 和 corosync 集群框架在內的超過 10,000 個新軟件包。
</p>

<p>數量眾多的可選軟件包使 Debian 保持了她一貫追求的目標--“通用操作系統”。Debian 適用於
各種常見使用案例：從桌面系統到上網本、從開發服務器到集群系統、數據以及存儲服務器。與此同時，對倉庫內
所有軟件包進行的自動測試（如安裝/升級測試）確保了 Debian 6.0 不辜負廣大用戶對 Debian 穩定版的
期盼。Debian 6.0 通過了苛刻的測試，品質堅若磐石。
</p>

<p>自 Debian 6.0 起，<q>Custom Debian Distribution</q> 重命名為
<a href="http://blends.alioth.debian.org/"><q>Debian Pure
Blends</q></a>。其覆蓋面也隨著 Debian 6.0 在原有的<a
href="http://wiki.debian.org/DebianEdu">Debian Edu</a>、<a
href="http://www.debian.org/devel/debian-med/">Debian Med</a> 和 <a
href="http://wiki.debian.org/DebianScience">Debian Science</a>
基礎之上增加了 <a
href="http://www.debian.org/devel/debian-accessibility/">Debian 
輔助功能</a>、<a href="http://debichem.alioth.debian.org/">DebiChem</a>、
<a href="http://wiki.debian.org/DebianEzGo">Debian EzGo</a>、<a
href="http://wiki.debian.org/DebianGis">Debian GIS</a> 和 <a
href="http://blends.alioth.debian.org/multimedia/tasks/index">Debian
多媒體</a>。所有相關的內容 <a href="http://blends.alioth.debian.org/">均可瀏覽</a>，
用戶也可以提名受歡迎的軟件包，使其可能被加入下一版本。</p>

<p>Debian 可通過許多種方式來安裝，如藍光光盤、DVD、CD 和 USB 儲存介質，也可通過網絡安裝。
GNOME 是默認的桌面環境，它被包含於第一張 CD 之中。其他桌面環境 &mdash; KDE Plasma 桌面和應用程序、
Xfce 或 LXDE &mdash; 可通過其他可選 CD 安裝。您也可以在 CD/DVD 的啟動菜單中選取希望使用的桌面環境。
Debian GNU/Linux 6.0 也再次提供同時支持多個硬件構架的 CD 和 DVD 映像，允許使用一張光盤在多種硬件平
臺上執行安裝作業。製作 USB 啟動介質的過程也被大幅簡化，詳情請參看
 <a href="$(HOME)/releases/squeeze/installmanual">安裝指南</a>。</p>

<p>除常規的安裝介質外，Debian GNU/Linux 也可以通過 Live 鏡像的方式直接使用而無需安裝。
Debian 提供用於 CD、USB 儲存介質和網絡啟動的 Live 鏡像，目前僅有用於 <code>amd64</code> 和 
<code>i386</code> 構架的鏡像。這些 Live 鏡像也可用於安裝 Debian GNU/Linux。</p>

<p>Debian GNU/Linux 6.0 的安裝過程有了多個方面的改進，包括更加易用的語言選擇和鍵盤設置、
對邏輯捲分區、RAID 和加密文件系統的支持。此外還添加了對 ext4 和 btrfs 文件系統的支持
&mdash; 在 kFreeBSD 構架上還有 ZFS 支持 &mdash;。目前 Debian 支持使用 70 種語言進行安裝。
</p>

<p>Debian 安裝鏡像現在已可通過
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (推薦)、
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> 或
<a href="$(HOME)/CD/http-ftp/">HTTP</a> 方式下載；更多信息請參看 
<a href="$(HOME)/CD/">Debian on CDs</a>。多個 <a href="$(HOME)/CD/vendors">供應商</a> 
也將很快開始提供 DVD、CD-ROM 以及藍光光盤。</p>

<p>從前一版本 Debian GNU/Linux 5.0 （代號 <q>Lenny</q>）升級到 Debian 6.0，在大多數情況下
均可使用 apt-get 軟件包管理工具自動處理，某種程度來說也可以用 aptitude 管理工具進行。一如往常，
Debian GNU/Linux 系統可以平穩無痛地升級，沒有任何必須的停機時間，但還是強烈建議事先
閱讀 <a href="$(HOME)/releases/squeeze/releasenotes">發行註記</a> 和 
<a href="$(HOME)/releases/squeeze/installmanual">安裝指南</a>，以便瞭解潛在會發生的問題，
並取得詳細的安裝和升級說明。本發行說明可能在發行之後的數週內持續更新，並將翻譯成其他語言。</p>


<h2>關於 Debian</h2>

<p>Debian 是一個自由的操作系統，由來自世界各地的數千位志願者，藉助互聯網並肩協作、傾心打造。Debian 項目
的原動力來自志願者的奉獻，執著對 Debian 社會契約的堅持以及信守打造最佳操作系統的承諾。Debian 6.0 的
發佈，是在這個方向上邁出的重要一步。</p>


<h2>聯繫信息</h2>

<p>更多信息，請訪問 Debian 項目首頁 
<a href="$(HOME)/">http://www.debian.org/</a> 或發送電子郵件至 
&lt;press@debian.org&gt;。</p>
