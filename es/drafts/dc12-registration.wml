# Status: [open-for-edit|content-frozen|sent]
# $Id: dc12-registration.wml 3537 2012-04-11 23:09:32Z madamezou $
# $Rev: 3537 $

<define-tag pagetitle>El registro para la DebConf12 se ha abierto</define-tag>

<define-tag release_date>2012-04-12</define-tag>
#use wml::debian::news
# #use wml::debian::translation-check translation="1.1" maintainer=""
<p>
El proyecto Debian se complace en anunciar que está abierto el
registro para la DebConf12, que se realizará en Managua, Nicaragua
del domingo 8 al sábado 14 de julio de 2012.
La conferencia será precedida por el DebCamp, del
domingo 1 de julio al sábado 7 de julio.
</p>
<p>
Igual que años anteriores, estarán disponibles tres opciones distintas 
de registro: <q>Patrocinado</q>, <q>Profesional</q> y <q>Corporativo</q>.
En la categoría <q>Patrocinado</q>, el proyecto Debian ayuda a personas
que no pueden pagar su estadía durante la DebConf. 
La categoría <q>Corporativa</q>, tiene un costo de USD 1300 (EUR 1000) y
está dirigida a aquellas personas que asisten a la DebConf como representantes
de compañias.
La categoría <q>Professional</q>, tiene un costo de USD 650 (EUR 500) y
está dirigida a personas o compañías que deseen ayudar a la conferencia
pagando los costos de su asistencia.
<br />
Será posible aplicar a subsidio de alimentación y estadía y/o de viaje hasta 
el 15 de mayo.
Para más información detallada sobre como registrarse, lea el <a
href="https://lists.debian.org/debian-devel-announce/2012/04/msg00002.html">anuncio
del equipo de la DebConf</a>.
</p>
<h2>Descuento de aerolínea</h2>
<p>
El equipo de DebConf ha conseguido un descuento en boletos aereos para 
participantes con SkyTeam a través de su programa de reuniones globales <q>Global Meetings</q>.
El descuento está disponible en la mayoría de las aerolíneas de SkyTeam 
para viajar del 26 de junio hasta el 20 de julio hacia y desde el aeropuerto
de Managua Nicaragua (MGA), para participar en la DebConf12.
Para ver los descuentos que ofrece SkyTeam para su viaje a DebConf12,
dirigase a  <a href="http://www.skyteam.com/GlobalMeetings">http://www.skyteam.com/GlobalMeetings</a>
e ingrese el ID del evento 2254S.
</p>
<h2>Call for papers</h2>
<p>
Debian le invita a presentar propuesta para charlas, presentaciones, sesiones de discusión,
y tutoriales para la Conferencia Debian 2012. 
Las propuestas no están limitadas a charlas tradicionales: podría proponer
una actuación, instalación artística, debate o algo más. Las propuestas oficiales serán
aceptadas hasta el 1 de junio de 2012, 23:59 UTC.
<br />
Este año, continuaremos organizando algunas charlas en tématicas. 
Si tienes una propuesta para una tématica, por favor contacta al equipo en
<a href="mailto:debconf-team@lists.debconf.org">debconf-team@lists.debconf.org</a>.
Ejemplos previos de tématicas incluyen <q>Debian Science</q>, <q>Debian
Infrastructure</q> y <q>Community Outreach</q>.
Si le gustaría ser coordinador de una tématica, puede solicitarlo en la lista
de correos.
Información detallada sobre como presentar un evento está disponible en el <a
href="https://lists.debian.org/debian-devel-announce/2012/04/msg00003.html">correo
electrónico del equipo de la DebConf</a>.
</p>

<h2>Acerca de la DebConf</h2>

<p>La DebConf es la conferencia anual del proyecto Debian. Además
de un programa lleno de charlas técnicas, sociales y políticas, la DebConf le
brinda la oportunidad a los desarrolladores, colaboradores y otras personas
interesadas, de conocerse en persona y trabajar juntos más estrechamente. Se
ha celebrado anualmente desde el año 2000 en diversos lugares alrededor 
del mundo.</p>

<p>Si usted tiene interés en apoyar a la DebConf como voluntario, por favor
contacte a <a href="mailto:debconf-team@lists.debconf.org">debconf-team@lists.debconf.org</a>
<br />
Si usted o su compañia está interesada en patrocinar DebConf
donando dinero o prestando equipos, por favor contacte al equipo
de patrocinio en <a href="mailto:sponsors@debconf.org">sponsors@debconf.org</a>.
<br />
Más información sobre la DebConf12 puede encontrarse en el <a href="http://debconf12.debconf.org/">
sitio web de la conferencia</a>.
</p>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian fue fundado en 1993 por Ian Murdock para ser un proyecto
de la comunidad verdaderamente libre. Desde entonces el proyecto ha crecido
hasta ser uno de los proyectos más grandes e importantes de software libre.
Miles de voluntarios en todo el mundo trabajan juntos para crear y mantener
programas para Debian. Se encuentra traducido en 70 idiomas y soporta una gran
cantidad de arquitecturas de computadoras, por lo que el proyecto se refiere a
si mismo como <q>el sistema operativo universal</q>.</p>

<h2>Información de contacto</h2>

<p>Para mayor información visite el sitio web de Debian en <a
href="http://www.debian.org/">http://www.debian.org/</a>, o envíe un correo a
&lt;press@debian.org&gt;.</p>

